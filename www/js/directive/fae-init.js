/**
 * Created by Nahuel Barrios <barrios.nahuel@gmail.com>.
 * Created on 20/04/14, at 18:55.
 */

quickAndEasyApp.directive('faeInit', [
    function () {
        console.log('Creating fae-init directive...');

        var link = function ($scope, element, attrs) {
            console.log('Executing fae-init directive...');

            /**
             * @param tabSelector The selector of the view that we want to display.
             */
            $scope.displayView = function (tabSelector) {
                console.log('Displaying view: ' + tabSelector);

                $viewsTab.find('a[href=' + tabSelector + ']').tab('show');

                switch (tabSelector) {
                    case '#panel-game':
                        $scope.app.view.game.init();
                        break;
                }
            };

            /**
             * Start corresponding view taking into account:
             * - If the user is already logged or not;
             */
            var start = function () {
                /**
                 * Fetch questions from main repository and save them locally.
                 */
                var fetchQuestions = function () {
                    var jsonUrl = 'https://spreadsheets.google.com/feeds/cells/' + MAIN_KEYS.QUESTIONS_GOOGLE_SPREADSHEET_KEY + '/'
                                      + MAIN_KEYS.QUESTIONS_GOOGLE_SPREADSHEET_NUMBER + '/public/basic?alt=json';
                    console.log('Fetching questions from: ' + jsonUrl);
                    var googleDocsSimpleParserConfiguration = {
                        url: jsonUrl,
                        done: function (questions) {
                            console.log('Obtained: ' + questions.length + ' questions.');
                            storage.set(storage.KEYS.QUESTIONS, questions);
                        },
                        fail: function (jqXHR, textStatus, errorThrown) {
                            console.log('There was an error getting questions: ' + jqXHR.status + '. Text: ' + textStatus);
                        }
                    };
                    googleDocsSimpleParser.parseSpreadsheetCellsUrl(googleDocsSimpleParserConfiguration);
                };

                var isUserAlreadyLogged = function () {
                    var user = storage.get(storage.KEYS.USER);

                    if (user) {
                        $scope.sync.user = $scope.sync.users.$child(user.id);
                        $scope.sync.user.$update(user);
                    }

                    return user && moment(user.token.expirationDate).isBefore(moment());
                };

                fetchQuestions();
                if (isUserAlreadyLogged()) {
                    console.log('User is already logged, skipping login view');
                    $scope.displayView(APPLICATION_KEYS.VIEWS.HOME);
                } else if (!mock.device) {
                    console.log('User is NOT logged, displaying login view');

                    facebook.plugin = new CC.CordovaFacebook();
                    facebook.plugin.init(facebook.app.id, facebook.app.name, facebook.app.permissions, function () {
                        console.log('Facebook plugin initialized successfully');
                    }, function () {
                        console.log('Facebook plugin could not be initialized');
                        //  TODO : Functionality : Close app??????
                    });
                }
            };

            var onDeviceReady = function () {
                console.log('Device is now ready, starting app...');

                $viewsTab = $('#viewsTabs');

                start();
            };

            var onBackButtonPressed = function (event) {
                event.preventDefault();

                var exitApp = function () {
                    navigator.app.exitApp();
                };

                switch ($viewsTab.find('>.nav.nav-tabs>li.active>a').attr('href')) {
                    case '#panel-login':
                    case '#panel-home':
                        exitApp();
                        break;
                    case '#panel-game':
                        $scope.app.view.game.handleBackButtonEvent();
                        break;
                    case '#panel-choose-friend':
                        $scope.app.view.chooseFriend.handleBackButtonEvent();
                }
            };

            if (mock.device) {
                onDeviceReady();
            } else {
                document.addEventListener('deviceready', onDeviceReady, false);
                document.addEventListener('backbutton', onBackButtonPressed, false);
            }
        };

        return {
            link: link
        };
    }
]);

