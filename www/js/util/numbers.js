/**
 * Created by Nahuel Barrios <barrios.nahuel@gmail.com>.
 * Created on 22/04/14, at 00:12.
 */
var numbers = (function () {

    /**
     * Generates a random number between the given two.
     * @param min
     * @param max
     * @returns {number}
     */
    var randomBetween = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };

    return {
        randomBetween: randomBetween
    };
}());