/**
 * Created by Nahuel Barrios <barrios.nahuel@gmail.com>.
 * Created on 19/04/14, at 19:52.
 */
var quickAndEasyApp = angular.module('quickAndEasyApp', ['ngTouch', 'firebase']);

// Log me in.
var rootStorageReference = new Firebase('https://' + MAIN_KEYS.FIREBASE_APP + '.firebaseio.com/');

rootStorageReference.auth(MAIN_KEYS.FIREBASE_SECRET, function (error) {
    if (error) {
        //  TODO : Functionality : Test it and DO SOMETHING!!
        console.log("Firebase login failed!", error);
    } else {
        console.log("Firebase login succeeded");

    }
});

var $viewsTab;