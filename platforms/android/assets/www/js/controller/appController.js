/**
 * Created by Nahuel Barrios <barrios.nahuel@gmail.com>.
 * Created on 14/04/14, at 22:42.
 */
quickAndEasyApp.controller('AppController', [
    '$scope', '$firebase', function ($scope, $firebase) {
        console.log('Creating AppController...');

        var notYetImplementedError = function (functionName, view) {
            console.log('Function ' + functionName + ' is not implemented for view: ' + view);
        };
        $scope.app = {
            name: MAIN_KEYS.APP_NAME,
            view: {
                game: {
                    init: notYetImplementedError(undefined, 'init', 'game'),
                    handleBackButtonEvent: notYetImplementedError.bind(undefined, 'handleBackButtonEvent', 'game')
                },
                chooseFriend: {
                    handleBackButtonEvent: notYetImplementedError.bind(undefined, 'handleBackButtonEvent', 'chooseFriend')
                }
            },
            liveGame: {
                play: false
            }
        };

        if (mock.storedUser) {
            $scope.sync = {
                user: mockData.storedUser
            };

            storage.set(storage.KEYS.USER, $scope.sync.user);
        } else {
            if (mock.login) {
                localStorage.removeItem(storage.KEYS.USER);
            }

            var storedUser = storage.get(storage.KEYS.USER);
            if (!storedUser) {
                //  Creates the object here because if not, then when modifing its value in child scopes doesn't update the view.
                storedUser = {};
            }

            $scope.sync = {
                user: storedUser
            };
        }

        //  Automatically syncs everywhere in realtime.
        $scope.sync.users = $firebase(rootStorageReference).$child('users');
    }
]);