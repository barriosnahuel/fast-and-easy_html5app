/**
 * Created by Nahuel Barrios <barrios.nahuel@gmail.com>.
 * Created on 18/04/14, at 02:45.
 */
quickAndEasyApp.factory('userService', [
    function () {
        console.log('Creating user service...');

        /**
         *
         * @param onError
         * @param onSuccess Callback that receives the returned token with: access_token and other parameters.
         */
        var login = function (onError, onSuccess) {
            var loginUsingFacebook = function () {
                facebook.plugin.login(onSuccess, function (err) {
                    console.log('Facebook plugin did NOT work as expected, error: ' + err);
                    onError(err);
                });
            };

            loginUsingFacebook();
        };

        var create = function ($scopeUser, facebookUser, token) {
            $scopeUser.$set({
                                id: facebookUser.id,
                                email: facebookUser.email,
                                name: facebookUser.name,
                                firstName: facebookUser.first_name,
                                lastName: facebookUser.last_name,
                                token: token,
                                score: {
                                    personal: 0,
                                    challenge: 0
                                }
                            });

            console.log('Saving user basic info to storage...');
            storage.set(storage.KEYS.USER, $scopeUser);
        };

        var update = function (user) {
            storage.set(storage.KEYS.USER, user);
        };

        var addQuestionToExcludedOnes = function (user, questionId) {
            user.$child('answeredQuestions').$child(questionId).$set(questionId);

            storage.set(storage.KEYS.USER, user);
        };

        var hasToAnswer = function (user, question) {
            return !user.answeredQuestions || !user.answeredQuestions[question.id];
        };

        return {
            login: login,
            create: create,
            update: update,
            addQuestionToExcludedOnes: addQuestionToExcludedOnes,
            hasToAnswer: hasToAnswer
        };
    }
]);