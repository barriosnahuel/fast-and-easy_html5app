/**
 * Created by Nahuel Barrios <barrios.nahuel@gmail.com>.
 * Created on 18/04/14, at 04:47.
 */
quickAndEasyApp.controller('LoginViewController', [
    '$scope', 'userService', function ($scope, userService) {
        console.log('Creating LoginViewController...');

        var afterLogin = function (next, token) {
            console.log('Facebook login successful');
            facebookLoginButton.button('reset');

            var afterGettingBasicInfo = function (data) {
                console.log('Obtained Facebook information for user: ' + data.email);

                var afterGettingFriends = function (data) {
                    console.log('User friends retrieved successfully');

                    var friends = data.data;
                    console.log('Obtained friends: ' + friends.length);

                    //  TODO : Functionality : Merge old-new friends (save its profile)
                    var friendsAlreadyPlaying = function (eachFriend) {
                        var isAlreadyPlaying = false;

                        for (var eachProperty in $scope.sync.users) {
                            if ($scope.sync.users.hasOwnProperty(eachProperty) && eachFriend.id == eachProperty) {
                                isAlreadyPlaying = true;
                                break;
                            }
                        }

                        console.log('Returning ' + isAlreadyPlaying + ' for friend: ' + eachFriend.name);
                        return isAlreadyPlaying;
                    };
                    friends = friends.filter(friendsAlreadyPlaying);

                    console.log('After filtering friends: ' + friends.length);

                    var $friends = $scope.sync.user.$child('friends');
                    for (var i = 0; i < friends.length; i++) {
                        var eachFriend = friends[i];

                        $friends.$child(eachFriend.id).$set($scope.sync.users[eachFriend.id]);
                    }
                };

                $scope.sync.user = $scope.sync.users.$child('' + data.id);
                if ($scope.sync.user.id) {
                    console.log('User is already registered as player, profile get from server.');
                } else {
                    console.log('Creating user for first time...');
                    $scope.sync.user.$set({
                                              id: data.id,
                                              email: data.email,
                                              name: data.name,
                                              firstName: data.first_name,
                                              lastName: data.last_name,
                                              token: token,
                                              score: {
                                                  personal: 0,
                                                  challenge: 0
                                              }
                                          });
                }

                if (mock.friends) {
                    afterGettingFriends(mockData.user.friends);
                } else {
                    $.get(facebook.url.friends(token.accessToken)).done(afterGettingFriends).fail(function (jqXHR, textStatus, errorThrown) {
                        console.log('Could NOT get user Facebook friends, error: ' + errorThrown);
                        //  TODO : Functionality : Do something when no friends were obtained!!!
                    });
                }

                next();
            };

            token.expirationDate = moment().add('s', token.expirationTime);

            if (mock.user) {
                afterGettingBasicInfo(mockData.user.basicInfo);
            } else {
                facebook.plugin.info(afterGettingBasicInfo, function (jqXHR, textStatus, errorThrown) {
                    console.log('Could NOT get user information, error: ' + errorThrown);
                });
            }
        };

        var onClickLogin = function (next) {
            userService.login(function (err) {
                console.log('Could NOT login user, err: ' + err);
                alert('Ocurrió un error intentando iniciar sesión, por favor contacte al desarrollador.');
            }, afterLogin.bind(undefined, next));
        };

        var $currentView, facebookLoginButton;

        if (mock.login) {
            onClickLogin = function (next) {
                afterLogin(next, mockData.user.token);
            };
        }

        $scope.onLoginButtonClick = function () {
            $currentView = $viewsTab.find($viewsTab.find('.nav.nav-tabs>.active>a').attr('href'));
            facebookLoginButton = $currentView.find('button');
            facebookLoginButton.button('loading');

            onClickLogin($scope.displayView.bind(undefined, '#panel-home'));
        };

    }
]);