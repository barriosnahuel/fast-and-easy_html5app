/**
 * Created by Nahuel Barrios <barrios.nahuel@gmail.com>.
 * Created on 19/04/14, at 21:15.
 */
quickAndEasyApp.controller('HomeViewController', [
    '$scope', function ($scope) {
        console.log('Creating HomeViewController...');

        $scope.playAlone = function () {
            $scope.displayView(APPLICATION_KEYS.VIEWS.GAME);
        };

        $scope.chooseFriend = function () {
            $scope.displayView(APPLICATION_KEYS.VIEWS.CHOOSE_FRIEND);
        };
    }

]);