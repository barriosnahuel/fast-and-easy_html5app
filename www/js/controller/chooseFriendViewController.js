/**
 * Created by Nahuel Barrios <barrios.nahuel@gmail.com>.
 * Created on 16/04/14, at 19:34.
 */
quickAndEasyApp.controller('ChooseFriendViewController', [
    '$scope', function ($scope) {
        console.log('Creating ChooseFriendViewController...');

        //  Sets the default sorting for friends list.
        $scope.predicate = 'name';

        $scope.app.view.chooseFriend.handleBackButtonEvent = function () {
            $scope.displayView(APPLICATION_KEYS.VIEWS.HOME);
        };

        $scope.startGame = function (opponent) {
            console.log('Starting game against: ' + opponent.name);

            $scope.app.liveGame.play = opponent ? true : false;
            $scope.app.liveGame.opponent = opponent;
            $scope.displayView(APPLICATION_KEYS.VIEWS.GAME);
        };
    }
]);