/**
 * Created by Nahuel Barrios <barrios.nahuel@gmail.com>.
 * Created on 19/04/14, at 17:36.
 */
quickAndEasyApp.factory('questionService', [
    'userService', function (userService) {
        console.log('Creating question service...');

        var POSIBILITIES = 2;

        var generateRandomPosition = function () {
            var positionIndex = numbers.randomBetween(1, POSIBILITIES);

            var position;
            switch (positionIndex) {
                case 1:
                    position = APPLICATION_KEYS.ANSWERS_POSITIONS.LEFT;
                    break;
                case 2:
                    position = APPLICATION_KEYS.ANSWERS_POSITIONS.RIGHT;
                    break;
            }

            return position;
        };

        var createRound = function (user, currentGame) {
            var questions = storage.get(storage.KEYS.QUESTIONS) || [];
            console.log('Obtained questions from storage: ' + questions.length);

            var filteredQuestions = questions.filter(userService.hasToAnswer.bind(undefined, user));
            console.log('Questions after filtering for this user: ' + filteredQuestions.length);

            if (filteredQuestions.length === 0) {
                //  TODO : Functionality : This user has no more questions to answer, DO SOMETHING!!!!
                console.log('WOOOOOOOOOOOOOOW, this user has no more questions to answer, DO SOMETHING!!!!');
                filteredQuestions = questions;
            }

            var firstQuestion = filteredQuestions[numbers.randomBetween(0, filteredQuestions.length - 1)];

            var round = {
                questionId: firstQuestion.id,
                question: firstQuestion.question
            };

            round.correctAnswerPosition = generateRandomPosition();
            switch (round.correctAnswerPosition) {
                case APPLICATION_KEYS.ANSWERS_POSITIONS.LEFT:
                    round.answerLeft = firstQuestion.answer;
                    round.answerRight = firstQuestion.wrongAnswer1;
                    break;
                case APPLICATION_KEYS.ANSWERS_POSITIONS.RIGHT:
                    round.answerLeft = firstQuestion.wrongAnswer1;
                    round.answerRight = firstQuestion.answer;
                    break;
            }

            currentGame.rounds.push(round);
            return currentGame;
        };

        return {
            createRound: createRound
        };
    }
]);