cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/com.ccsoft.plugin.CordovaFacebook/www/CordovaFacebook.js",
        "id": "com.ccsoft.plugin.CordovaFacebook.CordovaFacebook",
        "merges": [
            "CC"
        ]
    },
    {
        "file": "plugins/com.chariotsolutions.toast.plugin/www/phonegap-toast.js",
        "id": "com.chariotsolutions.toast.plugin.Toasty",
        "clobbers": [
            "toast"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "com.ccsoft.plugin.CordovaFacebook": "1.0.0",
    "com.chariotsolutions.toast.plugin": "1.1.1"
}
// BOTTOM OF METADATA
});