/**
 * Created by Nahuel Barrios <barrios.nahuel@gmail.com>.
 * Created on 18/04/14, at 14:28.
 */
var mode = {
    browser: false
};

var MAIN_KEYS = Object.freeze({
                                  APP_NAME: 'Fácil y Rápido',
                                  FACEBOOK_APP_ID: '739849546047336',
                                  QUESTIONS_GOOGLE_SPREADSHEET_KEY: '1HoF8XpPpvp4PRPLf3YlWeYP1aqF0xWKVFYyGS3r2yfw',
                                  QUESTIONS_GOOGLE_SPREADSHEET_NUMBER: 0,
                                  FIREBASE_APP: 'shining-fire-9782',
                                  /**
                                   * It is used to login client against Firebase. You will find it under "Secrets" tab.
                                   */
                                  FIREBASE_SECRET: 'ORjdLUjDSxITaiU6P8CtWkBriSNbw6JyfCypCZQg',
                                  TIME_TO_ANSWER: 3,
                                  CHALLENGE_SCORE_MULTIPLICATOR: 2.5
                              });

var APPLICATION_KEYS = Object.freeze({
                                         VIEWS: Object.freeze({
                                                                  LOGIN: '#panel-login',
                                                                  HOME: '#panel-home',
                                                                  GAME: '#panel-game',
                                                                  CHOOSE_FRIEND: '#panel-choose-friend'
                                                              }),
                                         ANSWERS_POSITIONS: Object.freeze({
                                                                              LEFT: 'left',
                                                                              RIGHT: 'right'
                                                                          })
                                     });


//  ----------------------------------------------------
//  Mock configuration for test different kind of things
//  Mock everything but storedUser to test it entirely on a browser.
var mock = {
    device: false,                  //  true to test in browsers.
    login: false,                   //  true to mock login. Prevents all uses of Facebook plugin.
    storedUser: false,              //  true to mock a stored user in localStorage.
    user: false,                    //  true to load dummy user with friends and score.
    friends: false,                 //  true to mock getting friends using Facebook Cordova plugin.
    liveGameFriendScoring: true,    //  true to mock remote friend scoring.
    notifications: false,           //  true to use browser's alert instead of Cordova notifications plugins.
};

if (mode.browser) {
    mock.device = true;
    mock.login = true;
    mock.user = true;
    mock.friends = true;
    mock.liveGameFriendScoring = true;
    mock.notifications = true;
}

if (mode.browser) {
    //  Add back buttons to specific views to be able to navigate the whole app.
    var $panelGame = $('#panel-game');
    $panelGame.find('#startGameModal .modal-footer').append('<button class="btn btn-primary btn-lg btn-block" data-ng-click="app.view.game.handleBackButtonEvent()"><i class="fa fa-mail-reply"></i>Volver</button>');
    $panelGame.find('#endGameModal .modal-footer').append('<button class="btn btn-primary" data-ng-click="app.view.game.handleBackButtonEvent()"><i class="fa fa-mail-reply"></i>Volver</button>');

    $('#panel-choose-friend').find('.row>.column>.row>.column:first').append('<button class="btn btn-primary btn-lg btn-block" data-ng-click="app.view.chooseFriend.handleBackButtonEvent()"><i class="fa fa-mail-reply"></i>Volver</button>');
}

//  ---------------------
//  Mock data for testing
var mockData = {
    user: {
        token: {
            accessToken: 'CAAKg46VhH2gBAKEWqoJFGZC8VxuqvtagLJrkvhxGNDdPZCMpt8ZCziT8ZCxRiIEuJXoJt5Ks3YdNnTQ2QZBda9NZCE6ZCrHNClY7HYNSukiT4ygeJUlv6nKDVyHs2CagKM76XyKDZAoSzHOGLb3vW8S6ZCQ28LypFFZCRXwgmEdUhkWO9Q9pfMwJA8cnFEswNXlByHeORqWx1J7fu4vIzwuQi1Fw9UkQ0IUBRueML7xMcw9wZDZD',
            expirationDate: '2014-04-23T03:41:01.519Z',
            permissions: [
                'photo_upload', 'publish_stream', 'share_item', 'video_upload', 'user_friends', 'email', 'installed', 'public_profile',
                'status_update', 'publish_checkins', 'create_note', 'basic_info', 'publish_actions'
            ]
        },
        basicInfo: {
            id: 100000430551896,
            email: 'barrios.nahuel@gmail.com',
            first_name: 'Nahuel',
            last_name: 'Barrios',
            name: this.first_name + ' ' + this.last_name
        },
        friends: {
            data: [
                {
                    id: 12414124151,
                    name: 'Anahi Barrios'
                },
                {
                    id: 124141215311,
                    name: 'Patricia Safranchik'
                },
                {
                    id: 1233166229,
                    name: 'Leandro Maresca'
                },
                {
                    id: 36324646,
                    name: 'Florencia Bilotta'
                },
                {
                    id: 3513951,
                    name: 'Nicolas Ortiz'
                },
                {
                    id: 93509250,
                    name: 'Martin Ortiz'
                },
                {
                    id: 183539581,
                    name: 'Sebastian Ortiz'
                },
                {
                    id: 14124109053,
                    name: 'Hernan Espeche'
                },
                {
                    id: 1248892223,
                    name: 'Mauro Mena'
                }
            ]
        }
    }
};

mockData.storedUser = (function () {
    var user = mockData.user.basicInfo;
    user.token = mockData.user.token;
    user.friends = mockData.user.friends;
    user.score = {
        personal: 8000,
        challenge: 2000
    };
    return user;
}());