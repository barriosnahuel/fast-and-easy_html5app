/**
 * Created by Nahuel Barrios <barrios.nahuel@gmail.com>.
 * Created on 15/04/14, at 22:50.
 */
quickAndEasyApp.controller('GameViewController', [
    '$scope', '$interval', 'questionService', 'userService', '$firebase', function ($scope, $interval, questionService, userService, $firebase) {
        console.log('Creating GameViewController...');

        var $view = $('#panel-game');
        var $endGameModal = $view.find('#endGameModal');
        var $startGameModal = $view.find('#startGameModal');
        var countdownPromise;
        var temporalScore;

        var resetView = function () {
            $endGameModal.modal('hide');

            $scope.currentGame = {
                score: 0,
                rounds: [],
                currentRound: {
                    questionId: -1
                }
            };

            if ($scope.app.liveGame.play) {
                $scope.currentGame.opponent = $scope.app.liveGame.opponent;
                $scope.currentGame.opponent.score = 0;

                $scope.sync.liveGames = $firebase(rootStorageReference).$child('liveGames');

                var liveGameId = $scope.sync.user.id + 'v' + $scope.currentGame.opponent.id;
                $scope.app.liveGame = $scope.sync.liveGames.$child(liveGameId);
                $scope.app.liveGame.$update({
                                                id: liveGameId,
                                                play: true,
                                                score: 0,
                                                opponent: $scope.currentGame.opponent
                                            });
            }

            temporalScore = 0;
            countdownPromise = undefined;
        };

        var initView = function () {
            $startGameModal.modal('show');

            resetView();
        };

        /**
         * Starts a new round by choosing a question and starting the countdown.
         */
        var startNewRound = function () {
            console.log('Starting new round...');

            /**
             * Loads a new question from stored ones.
             */
            var loadQuestion = function () {
                $scope.currentGame = questionService.createRound($scope.sync.user, $scope.currentGame);

                $scope.currentGame.currentRound = $scope.currentGame.rounds[$scope.currentGame.rounds.length - 1];
            };

            var startCountdown = function () {
                $scope.timer = MAIN_KEYS.TIME_TO_ANSWER;
                countdownPromise = $interval(function () {
                    if (--$scope.timer === 0) {
                        $interval.cancel(countdownPromise);
                        finishGame();
                    }
                    temporalScore = $scope.timer;
                }, 1000);
            };

            //  TODO : Functionality : Change starter question index
            loadQuestion();
            startCountdown();
        };

        /**
         * Finish the round (answer a question).
         * Tasks:
         * - Display success/error message for this question;
         * - Stop the countdown;
         * - Update score when neccessary;
         * - Starts a new round;
         * @param success
         */
        var finishRound = function (success) {
            var showEndRoundMessage = function (success) {
                var message = 'Mal =(';
                if (success) {
                    message = 'Bien =)';
                    userService.addQuestionToExcludedOnes($scope.sync.user, $scope.currentGame.currentRound.questionId);
                }

                if (mock.notifications) {
                    alert(message);
                } else {
                    toast.showShort(message);
                }
            };

            //  Stops countdown
            $interval.cancel(countdownPromise);

            showEndRoundMessage(success);
            if (success) {
                updateScore();
            }
            startNewRound();
        };

        var finishGame = function () {
            console.log('Finishing game...');
            //  TODO : Refactor :  This should be in a directive because it is DOM manipulation.
            $endGameModal.modal('show');

            $scope.playing = false;
            $scope.sync.user.$child('score').$child('personal').$set($scope.sync.user.score.personal + $scope.currentGame.score);

            if ($scope.app.liveGame.play) {
                $scope.sync.user.$child('score').$child('challenge').$set($scope.sync.user.score.challenge + $scope.currentGame.score);
            }

            userService.update($scope.sync.user);
        };

        /**
         * Setup game to start again after loosing one.
         */
        $scope.playAgain = function () {
            //  TODO : Refactor :  This should be in a directive because it is DOM manipulation.
            $endGameModal.modal('hide');
            updateScore(true);
            $scope.startGame();
        };

        /**
         * @param reset true to reset the score to its initial value to start a new game.
         */
        var updateScore = function (reset) {
            if (reset) {
                $scope.currentGame.score = 0;
                if ($scope.app.liveGame.play) {
                    $scope.app.liveGame.$child('score').$set(0);
                    $scope.app.liveGame.$child('opponent').$child('score').$set(0);
                }
            } else {
                var wonScore = temporalScore * 1000;
                if ($scope.app.liveGame.play) {
                    wonScore = wonScore * MAIN_KEYS.CHALLENGE_SCORE_MULTIPLICATOR;
                }

                $scope.currentGame.score += wonScore;
                if ($scope.app.liveGame.play) {
                    $scope.app.liveGame.$child('score').$set($scope.currentGame.score);

                    if (mock.liveGameFriendScoring) {
                        $scope.app.liveGame.$child('opponent').$child('score').$set($scope.app.liveGame.opponent.score + 4500);
                    }
                }
            }
        };


        $scope.timer = MAIN_KEYS.TIME_TO_ANSWER;

        $scope.app.view.game.init = initView;

        $scope.startGame = function () {
            $scope.playing = true;
            startNewRound();
        };

        $scope.app.view.game.handleBackButtonEvent = function () {
            if (!$scope.playing) {
                //  TODO : Refactor :  This should be in a directive because it is DOM manipulation.
                $scope.app.startGameAgainst = false;
                $startGameModal.modal('hide');
                resetView();
                $scope.displayView(APPLICATION_KEYS.VIEWS.HOME);
            }
        };

        $view.hammer().on('dragend', function (event) {
            event.preventDefault();

            if ($scope.playing) {
                console.log('Touch event detected: drag, direction: ' + event.gesture.direction);

                finishRound(event.gesture.direction === $scope.currentGame.currentRound.correctAnswerPosition);
            }
        });
    }
]);
