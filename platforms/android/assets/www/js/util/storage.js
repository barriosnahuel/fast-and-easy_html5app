/**
 * Created by Nahuel Barrios <barrios.nahuel@gmail.com>.
 * Created on 18/04/14, at 14:14.
 */
var storage = (function () {

    /**
     * Keys that I need to transfrom again to numbers (int or floats) because on localStorage are saved as strings.
     * @type {string[]}
     */
    var KEYS_TO_TRANSFORM = [
        'id', 'personal', 'challenge'
    ];

    var get = function (key) {
        var reviver = function (key, value) {
            var transformed = value;

            if (key !== '' && KEYS_TO_TRANSFORM.indexOf(key) >= 0) {
                var test = parseFloat(value);
                if (test) {
                    transformed = test;
                }
            }

            return transformed;
        };

        return JSON.parse(localStorage.getItem(key), reviver);
    };
    var set = function (key, object) {
        return localStorage.setItem(key, JSON.stringify(object));
    };

    return {
        get: get,
        set: set,
        KEYS: Object.freeze({
                                USER: 'user',
                                QUESTIONS: 'questions'
                            })
    };
}());