/**
 * Created by Nahuel Barrios <barrios.nahuel@gmail.com>.
 * Created on 18/04/14, at 14:15.
 */
var facebook = (function () {
    /**
     * When sending access_token URL parameter it MUST be HTTPS.
     * @type {string}
     */
    var GRAPH_API_URL = 'https://graph.facebook.com';
    var resources = Object.freeze({
                                      friends: '/me/friends'
                                  });

    var getUserFriends = function (accessToken) {
        return  GRAPH_API_URL + resources.friends + '?access_token=' + accessToken;
    };

    return {
        app: {
            id: MAIN_KEYS.FACEBOOK_APP_ID,
            name: MAIN_KEYS.APP_NAME,
            permissions: [
                'basic_info', 'email', 'publish_actions', 'user_friends'
            ]
        },
        url: {
            friends: getUserFriends
        }
    };
}());